package models

import (
	"github.com/google/uuid"
	"gorm.io/gorm"
)

// User represents a user in the system.
type User struct {
	gorm.Model
	ID      string `gorm:"primary_key"`
	Name    string `gorm:"not null"`
	Surname string `gorm:"not null"`
	Email   string `gorm:"not null;unique"`
	Role    string `gorm:"not null"`
}

// BeforeCreate is a callback that gets called before creating.
func (u *User) BeforeCreate(tx *gorm.DB) (err error) {
	u.ID = uuid.NewString()
	return nil
}
