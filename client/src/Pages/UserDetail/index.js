import React from "react";
import { useQuery} from "react-query"
import { useParams, useNavigate } from "react-router-dom"
import { fetchUser } from "../../api";
import {Box, Button, Text, Link} from '@chakra-ui/react';
import DeleteUser from "../DeleteUser";

function UserDetail() {

    const navigate = useNavigate()

    let {user_id} = useParams();

    console.log(user_id)

    const {isLoading, isError, data} = useQuery(user_id, () => 
    fetchUser(user_id));

    if (isLoading) {
        return (
            <div>
                Loading....
            </div>
        )
    }

    if (isError) {
        return (
            <div>
                Error.
            </div>
        )
    }

    console.log(data)

    return (
        <div>
            <Text as="h2" fontSize="2xl">
                {"Name: "}{data.name}
            </Text>
            <Text as="h2" fontSize="2xl">
                {"Surname: "}{data.surname}
            </Text>
            <Text as="h2" fontSize="2xl">
                {"Email: "}{data.email}
            </Text>
            <Text as="h2" fontSize="2xl">
                {"Role: "}{data.role}
            </Text>

            <Link>
                <Button colorScheme="pink" onClick={() => <DeleteUser/>}>
                    Delete
                </Button>
            </Link>
            

            <Link to="/users">
                <Button colorScheme="pink" onClick={() => navigate(-1)}>
                    Back
                </Button>
            </Link> 
        </div>
    )
}

export default UserDetail;