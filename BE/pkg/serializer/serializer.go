package serializer

import (
	"BE/internal/entity/dtos"
	"BE/internal/entity/models"
)

// ConvertDtoToModel converts user dto to user Model.
func ConvertDtoToModel(userDto *dtos.User) *models.User {
	return &models.User{
		Name:    *userDto.Name,
		Surname: *userDto.Surname,
		Email:   *userDto.Email,
		Role:    *userDto.Role,
	}
}

// ConvertModelToDto converts user Model to user dto.
func ConvertModelToDto(userModel *models.User) *dtos.User {
	return &dtos.User{
		ID:      userModel.ID,
		Name:    &userModel.Name,
		Surname: &userModel.Surname,
		Email:   &userModel.Email,
		Role:    &userModel.Role,
	}
}

// ConvertModelListToDtoList converts user Model list to user dto list.
func ConvertModelListToDtoList(userModelList []*models.User) []*dtos.User {
	var userDtoList []*dtos.User
	for _, userModel := range userModelList {
		userDtoList = append(userDtoList, ConvertModelToDto(userModel))
	}
	return userDtoList
}
