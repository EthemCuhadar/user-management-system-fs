package handler

import (
	"BE/internal/entity/dtos"
	"BE/internal/service"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/go-openapi/strfmt"
)

// UserHandler struct for user handler
type UserHandler struct {
	UserService service.IUserService
}

// NewUserHandler returns new user handler
func NewUserHandler(r *gin.RouterGroup, userService service.IUserService) {
	uh := UserHandler{
		UserService: userService,
	}

	r.POST("/create", uh.CreateUser)
	r.GET("/:id", uh.GetUserByID)
	r.GET("/list", uh.ListAllUsers)
	r.DELETE("/delete/:id", uh.DeleteUserByID)
	r.PUT("/update/:id", uh.UpdateUserByID)
	r.GET("/healthcheck", uh.HealthCheck)
}

// CreateUser takes a user from request, validates and send it to the service
// to create a new user in the database.
func (uh *UserHandler) CreateUser(c *gin.Context) {

	userBody := &dtos.User{}

	// Read user from request
	if err := c.ShouldBindJSON(userBody); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// Validate user
	if err := userBody.Validate(strfmt.NewFormats()); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// Response from service
	user, err := uh.UserService.CreateUser(userBody)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	// Send response
	c.JSON(http.StatusCreated, user)
}

// GetUserByID returns a user by id from the database
func (uh *UserHandler) GetUserByID(c *gin.Context) {
	id := c.Param("id")

	user, err := uh.UserService.GetUserByID(id)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusOK, user)
}

// ListAllUsers returns all users from the database
func (uh *UserHandler) ListAllUsers(c *gin.Context) {
	users, err := uh.UserService.ListAllUsers()
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusOK, users)
}

// DeteleUserByID deletes a user by id
func (uh *UserHandler) DeleteUserByID(c *gin.Context) {
	id := c.Param("id")

	err := uh.UserService.DeleteUserByID(id)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusOK, gin.H{"message": "User deleted"})
}

// UpdateUserByID updates a user by id
func (uh *UserHandler) UpdateUserByID(c *gin.Context) {
	id := c.Param("id")

	userBody := &dtos.User{}

	// Read user from request
	if err := c.ShouldBindJSON(userBody); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// Validate user
	if err := userBody.Validate(strfmt.NewFormats()); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// Response from service
	user, err := uh.UserService.UpdateUserByID(id, userBody)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	// Send response
	c.JSON(http.StatusOK, user)
}

// HealthCheck returns health check message for api
func (uh *UserHandler) HealthCheck(c *gin.Context) {
	healthcheck := &dtos.HealthcheckResponse{
		Environment: "development",
		Version:     "1.0.0",
		Status:      "OK",
	}

	c.JSON(http.StatusOK, healthcheck)

}
