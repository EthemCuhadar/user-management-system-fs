package config

import (
	"errors"
	"log"

	"github.com/spf13/viper"
	"go.uber.org/zap"
)

// Config is the configuration for the application.
type Config struct {
	ServerConfig ServerConfig
	DBConfig     DBConfig
}

// ServerConfig struct contains configuration parameters in terms of server.
type ServerConfig struct {
	AppVersion       string
	Mode             string
	RoutePrefix      string
	Debug            bool
	Port             string
	TimeoutSecs      int64
	ReadTimeoutSecs  int64
	WriteTimeoutSecs int64
}

// DBConfig struct contains configuration parameters in terms of database connection.
type DBConfig struct {
	DataSourceName string
	Name           string
	MaxOpen        int
	MaxIdle        int
	MaxLifetime    int
}

// LoadConfig takes configuation file path and returns necessary configuation
// parameters as Config struct.
func LoadConfig(filename string) (*Config, error) {
	v := viper.New()

	v.SetConfigName(filename)
	v.AddConfigPath(".")
	v.AutomaticEnv()
	if err := v.ReadInConfig(); err != nil {
		if _, ok := err.(viper.ConfigFileNotFoundError); ok {
			zap.L().Error("Config file not found", zap.Error(err))
			return nil, errors.New("config file not found")
		}
		return nil, err
	}

	var c Config
	err := v.Unmarshal(&c)
	if err != nil {
		zap.L().Error("Unmarshal error", zap.Error(err))
		log.Printf("unable to decode into struct, %v", err)
		return nil, err
	}

	return &c, nil

}
