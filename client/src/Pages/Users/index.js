import React from "react";
import User from "../../Components/User"
import { Grid, Box } from "@chakra-ui/react"
import { useQuery} from "react-query"

import { fetchUserList } from "../../api"

function Users() {

    const { isLoading, error, data } = useQuery('users', fetchUserList)

    if (isLoading) return 'Loading...'

    if (error) return 'An error has occurred: ' + error.message

    console.log(data)

    return (
    <div>
        
        <Grid templateColumns='repeat(1, 1fr)' gap={2}>
            {
                data.map((item, key) => (<User key={key} item={item} />))
            }
        </Grid>   
    </div>
    )
}

export default Users;