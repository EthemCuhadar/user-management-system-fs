# User Management System FS

This is a simple FullStack User Management System project which is written in Go programming language and ReactJS.

![Golang Image](BE/pkg/pictures/golang.png)

## 1. Description

The application is FullStack User Management System REST API which have following features

* **Create User**

Admin can create a product.

* **List All Users**

User, admin and guests can list the products from database.

* **Search User by ID**

User, admin and guests can search a product in the database with product name and product ID.

* **Delete Product by ID**

Admin can delete single product from database.

* **Update Product by ID**

Admin can update a product in the database.

## 2. Usage

* First prepare your configuration file from ./pkg/config/config-local.yaml

```yaml
ServerConfig:
  AppVersion: 1.0.0
  Mode: Development
  RoutePrefix: /api/v1/users
  Debug: false
  Port: ########
  TimeoutSecs: ########
  ReadTimeoutSecs: ########
  WriteTimeoutSecs: #######

DBConfig:
  DataSourceName: #######
  Name: ######
  MaxOpen: ######
  MaxIdle: ######
  MaxLifetime: ######
```

* Prepare your database(Postgres)

* Run BE with in BE file;

* ```terminal
  $ task run
  ```

* Run client with in client file;

* ```terminal
  npm start
  ```

## 3. Project Details

The Project has 2 main parts:

* 1. BackEnd
  2. FrontEnd

FrontEnd is written in React and Backend service written in Golang.

-------------------------

**Backend View**

Cmd has server and main functions which the program started from. 

Database will connect to Postgres.

Internal has entities(model and dtos), handlers, service and repository.

Pkg is responsible for other files and folders such as config, middlewares and helpers.

------------------------------------------

The project has 3 layers which are handler layer, service layer and repository layer.

* Handler layer is responsible for communication with client. It receives requests and data from client. After that it validates them and send them into service layer.

* Service layer is the layer which is like a bridge between handler and repository layers. The main objective of service layer is data transformation between handler layer and repository layer. It has also responsible for convertion of data models.

* Final layer is repository layer which is responsible for communicating with database. It saves and sends data which are belongs to client.

--------------------------------

**Frontend View**

There are 2 main pages (views) on frontend side which are master view and detailed view. Master view will list all users in the database. This screen will assist CRUD operations for users. The buttons are **New**, **Update** and **Delete**.

Detailed view will show the fields of users as form. Forms will have 2 buttons which are **Action** and **Back**.

Action buttons will change with selection in master view. The mapping can be seen below.

* New: Create

* Edit: Save

* Delete: Delete

## 4. Technologies Used

The list of technologies (libraries, packages etc.) are:

* **BackEnd Technologies are:**
* Viper
* Zap
* GORM
* Go Postgres Driver
* PostgreSQL
* Google uuid
* Gin
* Swagger
* Postman
* Taskfile
* **FrontEnd Technologies are:**
* ReactJS
* Chakra UI
* React Router DOM
* React Query
* Axios

## 5. Swagger Schemas

The relative endpoints can be seen below screenshot which are created with Swagger.io

* **Api Healthcheck**

![swagger1](BE/pkg/pictures/swagger1.png)

* **User Endpoints**

![swagger2](BE/pkg/pictures/swagger2.png)

* **Healthcheck model**

![swagger3](BE/pkg/pictures/model1.png)

* **User model**

![swagger4](BE/pkg/pictures/model2.png)

* **Error model**

![swagger5](BE/pkg/pictures/model3.png)

* ## 6. Licence

**MIT Licence**
