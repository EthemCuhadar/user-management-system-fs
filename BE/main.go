package main

import (
	"fmt"

	"BE/cmd"
	"BE/pkg/config"
	"BE/pkg/logger"

	"go.uber.org/zap"
)

func main() {
	fmt.Println("User Management System Server starting...")

	// Initialize Zap logger
	logger.InitLogger()
	defer logger.Close()

	// Read and print the config parameters
	cfg, err := config.LoadConfig("./pkg/config/config-local")
	if err != nil {
		zap.L().Error("Unable to load config file", zap.Error(err))
	}
	fmt.Println("Config:", cfg)

	// Initialize the server
	cmd.InitServer(cfg)
}
