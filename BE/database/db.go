package database

import (
	"time"

	"go.uber.org/zap"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"

	"BE/pkg/config"
)

// Connect is the function which takes configuration parameters from config file
// and connect to Postgres database. Afterwards, it pings the database whether the
// database is alive or not.
func Connect(cfg *config.Config) (*gorm.DB, error) {
	zap.L().Info("Connecting to Postgres database...")

	db, err := gorm.Open(postgres.Open(cfg.DBConfig.DataSourceName), &gorm.Config{})
	if err != nil {
		zap.L().Error("Error while connecting to Postgres database", zap.Error(err))
		return nil, err
	}

	origin, err := db.DB()
	if err != nil {
		zap.L().Fatal("database.db.Connect()", zap.Error(err))
		return nil, err
	}

	origin.SetMaxIdleConns(cfg.DBConfig.MaxIdle)
	origin.SetMaxOpenConns(cfg.DBConfig.MaxOpen)
	origin.SetConnMaxLifetime(time.Duration(cfg.DBConfig.MaxLifetime) * time.Second)

	if err := origin.Ping(); err != nil {
		zap.L().Fatal("database.db.Connect() - cannot ping database", zap.Error(err))
		return nil, err
	}

	zap.L().Info("Connected to Postgres database")

	return db, nil
}

// Close is the function which closes the database connection.
func Close(db *gorm.DB) {
	zap.L().Info("Closing database connection...")
	origin, err := db.DB()
	if err != nil {
		zap.L().Fatal("database.db.Close()", zap.Error(err))
	}
	origin.Close()
	zap.L().Info("Closed database connection")
}
