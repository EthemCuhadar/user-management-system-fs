package serializer

import (
	"BE/internal/entity/dtos"
	"BE/internal/entity/models"
	"testing"

	"github.com/stretchr/testify/assert"
)

// TestConvertDtoToEntity tests the ConvertDtoToEntity function.
func TestConvertDtoToEntity(t *testing.T) {
	// Sample User DTO
	Name := "John"
	Surname := "Doe"
	Email := "johndoe@mail.com"
	Role := "admin"

	userDto := &dtos.User{
		Name:    &Name,
		Surname: &Surname,
		Email:   &Email,
		Role:    &Role,
	}

	// Sample User Entity
	userEntity := ConvertDtoToModel(userDto)

	// Assertions
	assert.Equal(t, "John", userEntity.Name)
	assert.Equal(t, "Doe", userEntity.Surname)
	assert.Equal(t, "johndoe@mail.com", userEntity.Email)
	assert.Equal(t, "admin", userEntity.Role)
}

// TestConvertEntityToDto tests the ConvertEntityToDto function.
func TestConvertEntityToDto(t *testing.T) {
	// Sample User Entity
	ID := "1"
	Name := "John"
	Surname := "Doe"
	Email := "johndoe@mail.com"
	Role := "admin"

	userEntity := &models.User{
		ID:      ID,
		Name:    Name,
		Surname: Surname,
		Email:   Email,
		Role:    Role,
	}

	// Sample User DTO
	userDto := ConvertModelToDto(userEntity)

	// Assertions
	assert.Equal(t, "John", *userDto.Name)
	assert.Equal(t, "Doe", *userDto.Surname)
	assert.Equal(t, "johndoe@mail.com", *userDto.Email)
	assert.Equal(t, "admin", *userDto.Role)
}

// TestConvertEntityListToDtoList tests the ConvertEntityListToDtoList function.
func TestConvertEntityListToDtoList(t *testing.T) {
	// Sample user 1
	sampleUser1 := &models.User{
		ID:      "1",
		Name:    "John",
		Surname: "Doe",
		Email:   "johndoe@mail.com",
		Role:    "admin",
	}

	// Sample user 2
	sampleUser2 := &models.User{
		ID:      "2",
		Name:    "Jane",
		Surname: "Doe",
		Email:   "janedoe@mail.com",
		Role:    "user",
	}

	// Sample User Entity List
	userEntityList := []*models.User{
		sampleUser1,
		sampleUser2,
	}

	// Sample User DTO List
	userDtoList := ConvertModelListToDtoList(userEntityList)

	// Assertions
	assert.Equal(t, "John", *userDtoList[0].Name)
	assert.Equal(t, "Doe", *userDtoList[0].Surname)
	assert.Equal(t, "johndoe@mail.com", *userDtoList[0].Email)
	assert.Equal(t, "admin", *userDtoList[0].Role)

	assert.Equal(t, "Jane", *userDtoList[1].Name)
	assert.Equal(t, "Doe", *userDtoList[1].Surname)
	assert.Equal(t, "janedoe@mail.com", *userDtoList[1].Email)
	assert.Equal(t, "user", *userDtoList[1].Role)
}
