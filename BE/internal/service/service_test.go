package service

import (
	"BE/internal/entity/dtos"
	"BE/internal/entity/models"
	repo "BE/internal/repository"
	"testing"
)

var (
	ID      = "1"
	Name    = "John"
	Surname = "Doe"
	Email   = "johndoe@mail.com"
	Role    = "admin"
)

var nameUpdate = "John Jr"

var userDtoUpdate = dtos.User{
	ID:      ID,
	Name:    &nameUpdate,
	Surname: &Surname,
	Email:   &Email,
	Role:    &Role,
}

var userEntity = models.User{
	ID:      ID,
	Name:    Name,
	Surname: Surname,
	Email:   Email,
	Role:    Role,
}

func Test_UserService_CreateUser(t *testing.T) {
	type fields struct {
		repo repo.IUserRepository
	}
	type args struct {
		user *dtos.User
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *dtos.User
		wantErr bool
	}{
		{
			name: "UserService_CreateUser_Success",
			fields: fields{
				repo: userMockRepository{
					users: []*models.User{},
				},
			},
			args: args{
				user: &dtos.User{
					ID:      ID,
					Name:    &Name,
					Surname: &Surname,
					Email:   &Email,
					Role:    &Role,
				},
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			u := UserService{
				UserRepository: tt.fields.repo,
			}
			_, err := u.CreateUser(tt.args.user)
			if (err != nil) != tt.wantErr {
				t.Errorf("UserService.CreateUser() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
		})
	}
}

func Test_UserService_ListUsers(t *testing.T) {
	type fields struct {
		repo repo.IUserRepository
	}
	type args struct {
		user *dtos.User
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *dtos.User
		wantErr bool
	}{
		{
			name: "UserService_ListUsers_Success",
			fields: fields{
				repo: userMockRepository{
					users: []*models.User{},
				},
			},
			args: args{
				user: &dtos.User{
					ID:      ID,
					Name:    &Name,
					Surname: &Surname,
					Email:   &Email,
					Role:    &Role,
				},
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			u := UserService{
				UserRepository: tt.fields.repo,
			}
			_, err := u.ListAllUsers()
			if (err != nil) != tt.wantErr {
				t.Errorf("UserService.ListUsers() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
		})
	}
}

func Test_UserService_GetUserByID(t *testing.T) {
	type fields struct {
		repo repo.IUserRepository
	}
	type args struct {
		id string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *dtos.User
		wantErr bool
	}{
		{
			name: "UserService_GetUserByID_Success",
			fields: fields{
				repo: userMockRepository{
					users: []*models.User{
						&userEntity,
					},
				},
			},
			args: args{
				id: "1",
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			u := UserService{
				UserRepository: tt.fields.repo,
			}
			_, err := u.GetUserByID(tt.args.id)
			if (err != nil) != tt.wantErr {
				t.Errorf("UserService.GetUserByID() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
		})
	}
}

func Test_UserService_UpdateUser(t *testing.T) {
	type fields struct {
		repo repo.IUserRepository
	}
	type args struct {
		id   string
		user *dtos.User
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "UserService_UpdateUser_Success",
			fields: fields{
				repo: userMockRepository{
					users: []*models.User{
						&userEntity,
					},
				},
			},
			args: args{
				id:   "1",
				user: &userDtoUpdate,
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			u := UserService{
				UserRepository: tt.fields.repo,
			}
			_, err := u.UpdateUserByID(tt.args.id, tt.args.user)
			if (err != nil) != tt.wantErr {
				t.Errorf("UserService.UpdateUser() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
		})
	}
}

func Test_UserService_DeleteUser(t *testing.T) {
	type fields struct {
		repo repo.IUserRepository
	}
	type args struct {
		id string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "UserService_DeleteUser_Success",
			fields: fields{
				repo: userMockRepository{
					users: []*models.User{
						&userEntity,
					},
				},
			},
			args: args{
				id: "1",
			},
			wantErr: false,
		},

		// {
		// 	name: "UserService_DeleteUser_Error",
		// 	fields: fields{
		// 		repo: userMockRepository{
		// 			users: []*models.User{
		// 				&userEntity,
		// 			},
		// 		},
		// 	},
		// 	args: args{
		// 		id: "2",
		// 	},
		// 	wantErr: true,
		// },
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			u := UserService{
				UserRepository: tt.fields.repo,
			}
			err := u.DeleteUserByID(tt.args.id)
			if (err != nil) != tt.wantErr {
				t.Errorf("UserService.DeleteUser() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
		})
	}
}

type userMockRepository struct {
	users []*models.User
}

func (r userMockRepository) CreateUser(user *models.User) (*models.User, error) {
	r.users = append(r.users, user)
	return user, nil
}

// DeleteUser deletes a user with the given id from users slice.
func (r userMockRepository) DeleteUserByID(id string) error {
	for i, user := range r.users {
		if user.ID == id {
			r.users = append(r.users[:i], r.users[i+1:]...)
			return nil
		}
	}
	return nil
}

// GetUser returns a user with the given id from users slice.
func (r userMockRepository) GetUserByID(id string) (*models.User, error) {
	for _, user := range r.users {
		if user.ID == id {
			return user, nil
		}
	}
	return nil, nil
}

// ListUsers returns all users from users slice.
func (r userMockRepository) ListAllUsers() ([]*models.User, error) {
	return r.users, nil
}

// UpdateUser updates a user with the given id from users slice.
func (r userMockRepository) UpdateUserByID(id string, user *models.User) (*models.User, error) {
	for i, u := range r.users {
		if u.ID == id {
			r.users[i] = user
			return user, nil
		}
	}
	return nil, nil
}
