package service

import (
	"BE/internal/entity/dtos"
	"BE/internal/repository"
	"BE/pkg/serializer"
)

// UserService struct for user service
type UserService struct {
	UserRepository repository.IUserRepository
}

// NewUserService returns new user service
func NewUserService(userRepository repository.IUserRepository) UserService {
	return UserService{
		UserRepository: userRepository,
	}
}

// IUserService interface for user service
type IUserService interface {
	CreateUser(user *dtos.User) (*dtos.User, error)
	ListAllUsers() ([]*dtos.User, error)
	DeleteUserByID(id string) error
	GetUserByID(id string) (*dtos.User, error)
	UpdateUserByID(id string, user *dtos.User) (*dtos.User, error)
}

// CreateUser takes a user dto from handler function and converts it to a user model
// via serializer function.Afterwards it calls the user repository to create the
// user in the database.
func (s *UserService) CreateUser(user *dtos.User) (*dtos.User, error) {
	// Convert dto to model
	userModel := serializer.ConvertDtoToModel(user)

	// Response from database
	userModel, err := s.UserRepository.CreateUser(userModel)
	if err != nil {
		return nil, err
	}

	// Convert model to dto
	userDto := serializer.ConvertModelToDto(userModel)

	return userDto, nil
}

// GetUserByID takes a user id from handler function and calls the user repository
// to get the user from the database.
func (s *UserService) GetUserByID(id string) (*dtos.User, error) {
	// Response from database
	userModel, err := s.UserRepository.GetUserByID(id)
	if err != nil {
		return nil, err
	}

	// Convert model to dto
	userDto := serializer.ConvertModelToDto(userModel)

	return userDto, nil
}

// ListAllUsers returns all users from the database and send them to handler function
func (s *UserService) ListAllUsers() ([]*dtos.User, error) {
	// Response from database
	users, err := s.UserRepository.ListAllUsers()
	if err != nil {
		return nil, err
	}

	// Convert model to dto
	usersDto := serializer.ConvertModelListToDtoList(users)

	return usersDto, nil
}

// DeleteUserByID takes a user id from handler function and calls the user repository
// to delete the user from the database.
func (s *UserService) DeleteUserByID(id string) error {
	// Response from database
	err := s.UserRepository.DeleteUserByID(id)
	if err != nil {
		return err
	}

	return nil
}

// UpdateUserByID takes a user id and a user dto from handler function and calls the
// user repository to update the user in the database.
func (s *UserService) UpdateUserByID(id string, user *dtos.User) (*dtos.User, error) {
	// Convert dto to model
	userModel := serializer.ConvertDtoToModel(user)

	// Response from database
	userModel, err := s.UserRepository.UpdateUserByID(id, userModel)
	if err != nil {
		return nil, err
	}

	// Convert model to dto
	userDto := serializer.ConvertModelToDto(userModel)

	return userDto, nil
}
