package repository

import (
	"sync"

	"gorm.io/gorm"

	"BE/internal/entity/models"
)

// UserRepository struct for user repository
type UserRepository struct {
	DB *gorm.DB
	mu *sync.Mutex
}

// NewUserRepository returns new user repository
func NewUserRepository(mu *sync.Mutex, db *gorm.DB) UserRepository {
	return UserRepository{
		DB: db,
		mu: mu,
	}
}

// Migrations represents a migrations.
func (r *UserRepository) Migrations() {
	r.mu.Lock()
	defer r.mu.Unlock()

	r.DB.AutoMigrate(&models.User{})
}

// IUserRepository interface for user repository
type IUserRepository interface {
	CreateUser(user *models.User) (*models.User, error)
	GetUserByID(id string) (*models.User, error)
	ListAllUsers() ([]*models.User, error)
	DeleteUserByID(id string) error
	UpdateUserByID(id string, user *models.User) (*models.User, error)
}

// CreateUser creates a new user in the database
func (r *UserRepository) CreateUser(user *models.User) (*models.User, error) {
	r.mu.Lock()
	defer r.mu.Unlock()

	err := r.DB.Create(user).Error
	if err != nil {
		return nil, err
	}

	return user, nil
}

// GetUserByID returns a user by id from the database
func (r *UserRepository) GetUserByID(id string) (*models.User, error) {
	var user models.User
	err := r.DB.Where("id = ?", id).First(&user).Error
	if err != nil {
		return nil, err
	}

	return &user, nil
}

// ListAllUsers returns all users from the database
func (r *UserRepository) ListAllUsers() ([]*models.User, error) {
	var users []*models.User

	err := r.DB.Find(&users).Error
	if err != nil {
		return nil, err
	}

	return users, nil
}

// DeleteUserByID deletes a user by id
func (r *UserRepository) DeleteUserByID(id string) error {
	r.mu.Lock()
	defer r.mu.Unlock()

	err := r.DB.Where("id = ?", id).Delete(&models.User{}).Error
	if err != nil {
		return err
	}

	return nil
}

// UpdateUserByID updates a user by id with the given user
func (r *UserRepository) UpdateUserByID(id string, user *models.User) (*models.User, error) {
	r.mu.Lock()
	defer r.mu.Unlock()

	u := models.User{}

	result := r.DB.First(&u, "id = ?", id)
	if result.Error != nil {
		return nil, result.Error
	}

	u.Name = user.Name
	u.Email = user.Email
	u.Surname = user.Surname

	tx := r.DB.Save(&u)
	if tx.Error != nil {
		return nil, tx.Error
	}

	return user, nil
}
