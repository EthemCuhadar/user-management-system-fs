import React from "react";
import { useQuery} from "react-query"
import { useParams, useNavigate } from "react-router-dom"
import { deleteUser } from "../../api";
import {Box, Button, Text, Link} from '@chakra-ui/react';

function DeleteUser() {

    const navigate = useNavigate()

    let {user_id} = useParams();

    console.log(user_id)

    const {isLoading, isError, data} = useQuery(user_id, () => 
    deleteUser(user_id));

    if (isLoading) {
        return (
            <div>
                Loading....
            </div>
        )
    }

    if (isError) {
        return (
            <div>
                Error.
            </div>
        )
    }

    console.log(data)

    return (
        <div>
            <Text as="h2" fontSize="2xl">
                {data.message}
            </Text>

            <Button>
                Delete
            </Button>

            <Link to="/users/delete">
                <Button colorScheme="pink" onClick={() => navigate(-1)}>
                    Back
                </Button>
            </Link> 
        </div>
    )
}

export default DeleteUser;