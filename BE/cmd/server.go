package cmd

import (
	"BE/database"
	"BE/internal/handler"
	"BE/internal/repository"
	"BE/internal/service"
	"BE/pkg/config"
	"fmt"
	"net/http"
	"sync"
	"time"

	"github.com/gin-gonic/gin"
	"go.uber.org/zap"

	"github.com/gin-contrib/cors"
)

// InitServer initializes the server
func InitServer(cfg *config.Config) {

	// Connecting to the database
	DB, err := database.Connect(cfg)
	if err != nil {
		zap.L().Error("Error while connecting to database", zap.Error(err))
	}
	defer database.Close(DB)

	// Set gin mode as release
	gin.SetMode(gin.ReleaseMode)

	// Gin initialization
	r := gin.Default()

	r.Use(cors.New(cors.Config{
		AllowOrigins:     []string{"http://localhost:3000"},
		AllowMethods:     []string{"PUT", "PATCH", "GET", "DELETE"},
		AllowHeaders:     []string{"Origin"},
		ExposeHeaders:    []string{"Content-Length"},
		AllowCredentials: true,
		AllowOriginFunc: func(origin string) bool {
			return origin == "http://localhost:3000"
		},
		MaxAge: 12 * time.Hour,
	}))

	// Server parameters
	srv := &http.Server{
		Addr:         fmt.Sprintf(":%s", cfg.ServerConfig.Port),
		Handler:      r,
		ReadTimeout:  time.Duration(cfg.ServerConfig.ReadTimeoutSecs * int64(time.Second)),
		WriteTimeout: time.Duration(cfg.ServerConfig.WriteTimeoutSecs * int64(time.Second)),
	}

	// Router group for user management
	rootRouter := r.Group(cfg.ServerConfig.RoutePrefix)
	userRouter := rootRouter.Group("/users")

	// Create a mutex for the user repository
	var mu sync.Mutex

	// Repository initialization
	repository := repository.NewUserRepository(&mu, DB)

	// Migration initialization
	repository.Migrations()

	// User service initialization
	userService := service.NewUserService(&repository)

	// User handler initialization
	handler.NewUserHandler(userRouter, &userService)

	// Run the server
	if err := srv.ListenAndServe(); err != nil {
		zap.L().Error("Error while running the server", zap.Error(err))
	}
}
