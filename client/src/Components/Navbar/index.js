import React from "react";
import { Link } from "react-router-dom";
import styles from "./styles.module.css"
import { Button} from '@chakra-ui/react'
 
function Navbar() {
    return (
        <nav className={styles.nav}>
          <div className={styles.left}>
            <div className={styles.logo}>
              <Link to="/">User Management System</Link>
            </div>
            <ul className={styles.menu}>
                <li>
                    <Link to="/users">Users</Link>
                </li>
            </ul>
          </div>
          
          <div className="right">
            <Link to="/create">
              <Button colorScheme='pink'>New User</Button>
            </Link>
          </div>
        </nav>
    )
}

export default Navbar;