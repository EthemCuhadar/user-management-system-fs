import {Box, Button} from '@chakra-ui/react';
import { Link } from "react-router-dom"

function User({item}) {
    return (
        <Box borderWidth="1px" borderRadius="lg" overflow="hidden" p="3" >
            
            <Link to={`/users/${item.id}`}>
                <Box p="6">
                    <Box d="plex" alignItems="baseline" fontWeight="semibold">
                        {"Name: "} {item.name}
                    </Box>
                    
                    <Box d="plex" alignItems="baseline" fontWeight="semibold">
                        {"Surname: "} {item.surname}
                    </Box>

                    <Box d="plex" alignItems="baseline" fontWeight="semibold">
                        {"Email: "} {item.email}
                    </Box>

                    <Box d="plex" alignItems="baseline" fontWeight="semibold">
                        {"Role: "} {item.role}
                    </Box>

                </Box>
            </Link>

            <Button colorScheme="blue">Details</Button>
            <Button colorScheme="blue">Update</Button>
            <Button colorScheme="blue">Delete</Button>

        </Box>
    )
}

export default User;