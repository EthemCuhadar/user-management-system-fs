import './App.css';
import {
  BrowserRouter as Router,
  Route,
  Link,
  Routes
} from "react-router-dom";
import Navbar from "./Components/Navbar";
import CreateUser from "./Pages/CreateUser";
// import UpdateUser from "./Pages/UpdateUser";
import Users from "./Pages/Users";
import UserDetail from './Pages/UserDetail';
import DeleteUser from './Pages/DeleteUser';

function App() {
  return (
    <Router>
      <div>
        <Navbar/>

        <div id="content">
          <Routes>
            <Route path="/users" exact element={<Users/>} />
            <Route path="/users/:user_id" element={<UserDetail/>} />
            <Route path="/create" element={<CreateUser/>} />
            <Route path="/users/delete:user_id" element={<DeleteUser/>}></Route>
            {/* <Route exact path="/update" component={UpdateUser} /> */}
          </Routes>
        </div>
      </div>
    </Router>
  );
}

export default App;
