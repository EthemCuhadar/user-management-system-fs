import axios from "axios"

export const fetchUserList = async() => {
    const {data} = await axios.get("http://localhost:8080/api/v1/users/list")

    return data;
}

export const fetchUser = async(id) => {
    console.log(id)
    const {data} = await axios.get("http://localhost:8080/api/v1/users/" + id);

    return data;
}

export const deleteUser = async(id) => {
    console.log(id)
    const {data} = await axios.delete("http://localhost:8080/api/v1/users/delete/" + id);

    return data;
}